
const auth = require("../auth.js");
const bcrypt = require("bcrypt");
const { get } = require("mongoose");
const { all, param } = require("../routes/productRoutes.js");
const Product = require("../models/Product.js");

// creates a new product
module.exports.addProduct = (userData, productData) => 
{
    if (userData.isAdmin) 
    {
        let newProduct = new Product({
            name: productData.name,
            description: productData.description,
            price: productData.price
        });
        return newProduct.save().then((product, error) =>
        {
            if (error) 
            {
                console.log(error)
                return false;
            } 
            else 
            {
                return true;
            }
        })
    } 
    else 
    {
        return Promise.reject('Unauthorised user');
    }
};

// retreives all products
module.exports.getAllActive = () =>{
	return Product.find({ isActive: true }).then(result =>{
		return result;
	})
};
// retrieve a single product

module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};


// updates a product
module.exports.updateProduct = (reqParams,reqBody) => 
{

    let updateProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        stock: reqBody.stock
    }
   
    return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product,err) => {
        if(err) {
        return false;
    } else 
    {
        return true;
    }
});

};

// archive a product

module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive : !reqParams.body.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};
