const User = require("../models/User.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");
const { get } = require("mongoose");
const { all, param } = require("../routes/userRoutes.js");
const Product = require("../models/Product.js");


module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        if (result.length > 0){
           return true;
        } else {
            return false;
        }
    })
};

module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    return newUser.save().then((user,error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    })
};


module.exports.getProfile = (data) => {
	console.log(data);
	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};


module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result =>{
		if(result === null){
			return false;
		} else {
            
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect){
                return {access: auth.createAcessToken(result)}
            } else {
                return false;
            }
        }
    })
};

// admin can set a user as admin
module.exports.setAsAdmin = (reqParams,reqBody) => {
    let setAsAdmin = {
        isAdmin: true
    }
    
    return User.findByIdAndUpdate(reqParams.userId, setAsAdmin).then((course,err) => {
        if(err) {
        return false;
    } else 
    {
        return true;
    }
});
};

// create order

// total amount error AND fix non-admin

module.exports.createAnOrder = async (data) =>{

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({productId: data.productId,totalAmount: data.totalAmount});

		return user.save().then((user, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
	
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.order.push({userId: data.userId, totalAmount: data.totalAmount});

		return product.save().then((product, error)=>{
			if (error) {
				return false;
			}else{
				return true;
			}
		})
	});
	
	if (isUserUpdated && isProductUpdated) {
		return true;
	}else{
		return false; 
	}

};

// admin user retrieves all orders

module.exports.getAllOrders = (data) => {


        return User.find({}).then(result => {
            result.password = " ";

            return result;
        })
        
    
};


// retrieve authenticated user orders


module.exports.getMyOrders = (data) => {
	console.log(data);
	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};

// delete order

// module.exports.deleteOrder = (userId) => {

//     return User.findByIdAndRemove(userId.orders).then((result,error)=> {
// 		if(error){
// 			console.log(error);
// 			return false;
// 		}else{
// 			return result
// 		}
// 	})
// };





