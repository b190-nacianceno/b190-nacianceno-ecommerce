const express = require("express");
const router = express.Router();
const productController = require("../controller/productController.js");
const auth = require("../auth.js");

// adds a product
router.post("/", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization);
    productController.addProduct({isAdmin:userData.isAdmin},req.body).then(result => res.send(result)).catch(err => res.send(err));
    
});

// retrieves all active products
router.get('/', (req, res) =>{
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
} );

// retrieves a specific product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// updates a product
router.put("/:productId", auth.verify,(req,res) => {
    if(auth.decode(req.headers.authorization).isAdmin === false){
        res.send(false);
    } else {
    productController.updateProduct(req.params,req.body).then(resultFromController => res.send(resultFromController));
    }
});


// archives a course
router.put("/:productId/archive", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin !== true) {
		res.send(false);
	} else {
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	}
});




module.exports = router;
