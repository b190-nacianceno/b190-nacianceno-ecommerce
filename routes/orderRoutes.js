


const express = require("express");
const router = express.Router();
const orderController = require("../controller/userController.js");
const auth = require("../auth.js");
const { verify } = require("jsonwebtoken");

// Create cart
router.post("/shoppingCart", auth.verify, (req,res) => {
    const newCart = new Cart(req.body);
    orderController.createCart(req.body).then(resultFromController => res.send(resultFromController));
});

// router.post("/register", (req,res) => {
//     userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
// });

// router.get("/details", auth.verify, (req, res) => {
	
// 	const userData = auth.decode(req.headers.authorization);
// 	console.log(userData);
// 	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
// });


// router.post("/login", (req,res) => {
//     userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
// });

router.put("/:productId/", auth.verify,(req,res) => {
    if (auth.decode(req.headers.authorization).isAdmin === false){
        res.send(false);
    
} else {
    userController.setAsAdmin(req.params).then(resultFromController => res.send(resultFromController));
}

});

// CHECK AGAIN non-admin authenticated user creates an order
// router.post("/checkout", auth.verify, (req, res) => {
// 	let data = {
// 		userId : auth.decode(req.headers.authorization).id,
// 		productId : req.body.productId
// 	}

//     if(auth.decode(req.headers.authorization).isAdmin === true) {
// 		res.send("Administrators are not allowed to place an order.");
// 	} else {
// 	userController.createAnOrder(data).then(resultFromController => res.send(resultFromController));
//     }
// });

// // admin user retrieves all orders

// router.get("/orders", auth.verify,(req,res) => {
//     if(auth.decode(req.headers.authorization).isAdmin === false){
//         res.send(false);
//     } else {
//     userController.getAllOrders(req.params,req.body).then(resultFromController => res.send(resultFromController));
//     }
// });


// // logged in user retrieves their orders
// router.get("/myOrders", auth.verify, (req, res) => {
// 	// 
// 	const userData = auth.decode(req.headers.authorization);
// 	console.log(userData);
// 	userController.getMyOrders({userId : userData.id}).then(resultFromController => res.send(resultFromController));
// });


module.exports = router;