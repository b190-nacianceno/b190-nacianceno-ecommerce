const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const Product = require("./models/Product");

const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");


const app = express();

mongoose.connect("mongodb+srv://ezranacianceno:Bernadette12@cluster0.mliwlr2.mongodb.net/b190-Course-Booking?retryWrites=true&w=majority",
{
    useNewUrlParser: true,
    useUnifiedTopology: true
});


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open",() => console.log("we're connected to the database"));


app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// edit this to include new routes
app.use("/user", userRoutes);
app.use("/products", productRoutes);

app.get("/user", (req, res) => {
	res.send("Hello World");
});

app.listen(process.env.PORT||4000, () => {console.log(`API now online at port ${process.env.PORT||4000}`)})