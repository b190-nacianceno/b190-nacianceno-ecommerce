const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true,"Product name is required"]
    },
    description:{
        type: String,
        required:[true,"Description is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    isActive:{
        type: Boolean,
        default: true 
    },
    stock: {
        type: Number,
        default:100
    },
    createdOn:{
        type: Date,
        default: new Date()
    },
    order: [
        {
            userId: {
                type: String,
                required: [true, "User ID is required."]
            },
            totalAmount: {
                type: Number,
                required: [false,"Total amount is required"]
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            }
           
        }
    ]
});

module.exports = mongoose.model("Product", productSchema);